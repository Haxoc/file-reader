columnNumber = 6 # Could be user input
fileList = [] # Define the file list
file_name = "test.txt" # Change to fit your file

for i in range(0, columnNumber):
    
    # Add the new to become file names to the file list 
    fileList.append("column{}.txt".format(i))

    ## Loop through file list and create files for each name
    ## OVERWRITES ANY EXITING FILES WITH SAME FILE NAMES!
    with open(fileList[i], "w") as fileWrite:
            continue

        
# Opens target files using a 'with' statement

print("Opennig file...\n")
with open(file_name, 'r') as file:

    # Counts the lines in the file
    lineIndex = 0

    # Sets index in 'mini list' to 0(used to divide lines into columns)
    indexOutOf = 0

    # Loops through every line in the file
    print("Processing...\n")
    for line in file:
        #  Checks if the line is empty (contains only a return)
        if line == "\n":
            # Adds 1 to line count
            lineIndex += 1

            # Resets the line count in 'mini list'
            indexOutOf = 0
            continue

        #Otherwise, or if the line does contain text
        else:
            if 0 <= indexOutOf < len(fileList):
                
                # Opens the destination file with 'append' option
                with open(fileList[indexOutOf], "a") as fileWrite:
                    
                    # Writes the line to the new file
                    fileWrite.write(line)

            # Increments the 'mini list' counter
            indexOutOf += 1

            # For debugging purposes
            # print("IndexOutOf", indexOutOf)

        # Increments line count
        lineIndex += 1

        # For debugging purposes
        # print("Iteration", lineIndex)

print("Done.")
